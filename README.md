Mini Projet CV
==============

# 1. Objectifs

- Le mini-projet consistera à construire votre CV en utilisant Bootstrap
- Utiliser un outil comme figma/canvas pour crée un prototype de cv
- GIT et GITHUB doivent également être utilisés pour l'ensemble du projet



# 2. Etapes à réaliser 

- 01. Créer un dossier vide pour votre CV
- 02. Initialisez GIT dans ce dossier
- 03. Créez d'abord un fichier readme.md qui servira de page d'accueil pour le repo GITHUB *(vous pouvez simplement y ajouter un petit résumé du projet)*
- 04. Ajoutez les fichiers index.html, style.css
- 05. Ajoutez également un dossier pour les images
- 06. Bootstrap peuvent être téléchargés ou utiliser les liens CDN
- 07. Préparez un fichier pour ajouter à git ignore afin qu'il ne soit pas inclus dans le suivi GIT
- 08. Faites votre premier commit
- 09. Apportez des modifications à votre fichier index.html, Faites votre deuxième commit
- 10. Modifiez ensuite le fichier style.css pour ajouter votre propre CSS, effectuez à nouveau un autre commit
- 11. Effectuez de nouveaux commits regulier pour vos modifications
- 12. Modifiez les TAGS GIT pour les valeurs SHA-1 par défaut avec vos propres numéros de version comme :
    - MON_CV_V1.0.0
    - MON_CV_V1.0.1
    - MON_CV_V1.0.2
    - MON_CV_V1.1.0
    - MON_CV_V1.0.1 
- 13. Changer la branche MASTER (MAIN) pour une autre branche comme TEST

===
### JUSQU'ICI, TOUTES LES MODIFICATIONS SONT EFFECTUÉES UNIQUEMENT SUR LE DOSSIER GIT LOCAL

- 14. Créez maintenant un dépôt GITHUB (public) pour ce projet de CV
- 15. Lier votre dépôt local avec le distant
- 16. Utilisez GIT PUSH pour envoyer votre projet CV local sur GITHUB
- 17. Apporter d'autres modifications au projet avec la branche TEST 
- 18. Fusionnez la branche TEST avec la branche MASTER (MAIN)

------
- 19. >*De plus, vous pouvez ajouter du code Javascript / JQUERY ou tiliser une bibliothèque comme https://animate.style/ pour ajouter une animation CSS à votre CV*
------

- 20. Renvoyez-moi le lien vers votre dépôt GIT

## BONNE COURAGE !!!
